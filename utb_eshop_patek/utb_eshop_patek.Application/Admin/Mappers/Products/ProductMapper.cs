﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Application.Admin.Mappers.Products
{
    public class ProductMapper : IProductMapper
    {
        private readonly IMapper _mapper;

        public ProductMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Product GetEntityFromViewModel(ProductViewModel viewModel)
        {
            return _mapper.Map<Product>(viewModel);
        }

        public ProductViewModel GetViewModelFromEntity(Product entity)
        {
            return _mapper.Map<ProductViewModel>(entity);
        }

        public IList<ProductViewModel> GetViewModelsFromEntities(IList<Product> entities)
        {
            return _mapper.Map<IList<ProductViewModel>>(entities);
        }
    }
}
